function x = kepler(M,epsilon,tol)
  x_prev = M;
  x = x_prev - (x_prev - epsilon*sin(x_prev) -  M)/(1 - epsilon*cos(x_prev));
  while abs(x - x_prev) > tol
    x_prev = x;
    x = x_prev - (x_prev - epsilon*sin(x_prev) - M)/(1 - epsilon*cos(x_prev));
  end
end