function y = multinvhouse(V,x)
  y = size(V, 2);
  for i = 1:y
    x = hh_tr(V(:,i), x);
  end
  y = x;
end

function y = hh_tr(v, x)
  y = x - 2*v*(v' * x)/(v'*v);
end