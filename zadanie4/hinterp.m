function pp = hinterp(x, y, z)
%HINTERP Function compute Piecewise Cubic Hermite Interpolating Polynomial.
%
%    pp =  hinterp(x, y, z)
%
%    Return the Piecewise Cubic Hermite Interpolating Polynomial (pchip)
%    of points x and y, and derivatives z.
  
    % Transform vectors to column form
    x=x(:); y=y(:); z=z(:);
    
    % Check if the number of arguments is correct 
    if nargin < 3
        throw(error('Function needs 3 arguments.'));
    end
    % Vectors must have the same length
    if length(x) ~= length(y) || length(y) ~= length(z)
        throw(error('Vectors vary in length.'));
    end
    % Values in x must be unique
    if length(x) ~= length(unique(x))
        throw(error('Not all values in x are different.'));
    end
    % Check if values in x are sorted and if not emit warning and sort vectors
    if ~issorted(x)
        warning("Vector x is not sorted. Sorting vectors.");
        
        [x, index] = sort(x);
        y = y(index);
        z = z(index);
    end

    % Calculate coefficients
    coefs = ones(length(x)-1,4);
   
    dx = diff(x);
    dy = diff(y);
    
    coefs(:,4) = y(1:end-1);
    coefs(:,3) = z(1:end-1);
    coefs(:,2) = 3*dy./(dx.^2) - (2*z(1:end-1)+z(2:end))./dx;
    coefs(:,1) = - 2*dy./(dx.^3) + (z(1:end-1)+z(2:end))./(dx.^2);

    % Make piecewise polynomial
    pp = mkpp(x, coefs);
end
