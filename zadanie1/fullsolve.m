function X = fullsolve(b, eps)
    u = ones(length(b), 1) * eps;
    X = shmo(eps, u, b);
end

function X = shmo(eps,u,Y)
  Au = tridiag61(eps,u);
  Ay = tridiag61(eps,Y);
  X = (Ay - Au*sum(Ay)/(1+sum(Au)));
end

function X = tridiag61(eps, Y)
    n = length(Y);
    V = zeros(n,1);   
    X = zeros(n,1);
    A = 6 - eps;
    B = 1 - eps;
    
    k = A;
    X(1) = Y(1) / k;
    
    for i=2:n
        V(i-1) = B / k;
        k = A - B*V(i-1);
        X(i) = ( Y(i) - B*X(i-1) ) / k;
    end
    
    for j=n-1:-1:1
        X(j) = X(j) - V(j)*X(j+1);
    end
end
