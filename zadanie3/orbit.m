function [a, b, c, d, e] = orbit(x,y) 
    A = [y.^2, x.*y, x, y, ones(length(x), 1)];
    B = x.^2;
    C = A\B;
    
    a = C(1);
    b = C(2);
    c = C(3);
    d = C(4);
    e = C(5);
end